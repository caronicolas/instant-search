resource "azurerm_kubernetes_cluster" "instantsearchdemo" {
    name                    = "${var.aks_name}"
    location                = data.azurerm_resource_group.instantsearchdemo.location
    resource_group_name      = data.azurerm_resource_group.instantsearchdemo.name
    dns_prefix              = "instantsearchdemo"

    kubernetes_version      = var.aks_version

    network_profile {
        network_plugin      = "azure"
        service_cidr        = var.aks_services_cidr_block
        dns_service_ip      = var.aks_dns_service_ip
        docker_bridge_cidr  = var.aks_docker_bridge_cidr
        load_balancer_sku   = "Basic"
    }

    identity {
        type                = "SystemAssigned"
    }

    default_node_pool {
        name                = var.aks_node_pool_name
        node_count          = var.aks_node_count
        vm_size             = var.aks_vm_size
        type                = "VirtualMachineScaleSets"
        os_disk_size_gb     = 32
        vnet_subnet_id      = azurerm_subnet.aks.id
        max_pods            = 20
    }

    tags = {
        environment = "demo"
    }
}