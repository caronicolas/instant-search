resource "azurerm_public_ip" "nginx-ingress-controller" {
  name                = "nginx-ingress-controller"
  location            = data.azurerm_resource_group.instantsearchdemo.location
  resource_group_name = data.azurerm_resource_group.instantsearchdemo.name
  allocation_method   = "Static"
}
