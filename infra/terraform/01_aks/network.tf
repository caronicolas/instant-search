resource "azurerm_public_ip" "public-ip" {
  name                = "public-ip"
  location            = data.azurerm_resource_group.instantsearchdemo.location
  resource_group_name = data.azurerm_resource_group.instantsearchdemo.name
  allocation_method   = "Static"
}


resource "azurerm_virtual_network" "aks" {
    name                = "aks"
    address_space        = [var.aks_vnet_cidr_block]
    location            = data.azurerm_resource_group.instantsearchdemo.location
    resource_group_name = data.azurerm_resource_group.instantsearchdemo.name

    tags = {
        environment = "demo"
    }
}

resource "azurerm_subnet" "aks" {
    name                    = "aks"
    resource_group_name     = data.azurerm_resource_group.instantsearchdemo.name
    virtual_network_name    = azurerm_virtual_network.aks.name
    address_prefixes         = [var.aks_subnet_cidr_block]
}
