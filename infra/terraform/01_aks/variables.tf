variable "aks_name" {
    type = string
}

variable "aks_vnet_cidr_block" {
    type = string
}

variable "aks_services_cidr_block" {
    type = string
}

variable "aks_dns_service_ip" {
    type = string
}

variable "aks_docker_bridge_cidr" {
    type = string
}

variable "aks_node_pool_name" {
    type = string
}

variable "aks_node_count" {
    type = string
}

variable "aks_vm_size" {
    type = string
}

variable "aks_version" {
    type = string
}

variable "aks_subnet_cidr_block" {
    type = string
}

variable "location" {
  default = "West Europe"
}

variable "root_dns_zone_name" {
    type = string
}
