resource "azurerm_resource_group" "instantsearchdemo" {
  name     = "instantsearchdemo"
  location = var.location

  tags = {
    environment = "demo"
  }

  lifecycle {
    prevent_destroy = true
  }
}
