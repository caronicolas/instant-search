root_dns_zone_name          = "instantsearchdemocani.com"

# AKS

aks_version             = "1.22.4"
aks_vm_size             = "Standard_B4ms"
aks_name                = "instantsearchdemo"
aks_vnet_cidr_block     = "10.1.0.0/16"
aks_subnet_cidr_block   = "10.1.0.0/16"
aks_services_cidr_block = "10.0.32.0/19"
aks_dns_service_ip      = "10.0.32.10"
aks_docker_bridge_cidr  = "172.17.0.1/16"
aks_node_count          = "3"
aks_node_pool_name      = "instantsdemo"