FROM node:current-alpine3.15

WORKDIR /usr/src/app

COPY package.json /usr/src/app
RUN npm install

COPY . /usr/src/app

USER 1001

EXPOSE 3000
CMD ["npm", "start"]