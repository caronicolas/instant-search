HOW TO
====

# Deploy instant search demo locally

## Requirements

You need to be sure that you've a docker engine installed on your computer up and running.

You need to have docker compose installed on your computer, you can find a way to install it on this [link](https://docs.docker.com/compose/install/#install-compose).

## Deployment

To deploy the project locally, you need to create a docker image of your work. Open a terminal, go on the root of the project and simply launch the command:

```sh
docker build . [-t image_name:version_number]
```

If you want to run the project directly, you can use docker compose with the command:

```sh
docker-compose up
```

You must see a message to explain the different way to connect to the application.

# Deploy instant search demo on AKS

To deploy the project on AKS, you need to commit it on the master branch, then gitlab ci make the deployment directly.
You can follow the state of the deployment in gitlab UI or with the kubectl command:

```
watch kubectl get deploy -n instantsearchdemo
```
At the end, you need to create a port forwarding to establish a connection with your new deployment.
To do this, you can run this command:

```
kubectl port-forward service/instantsdemo 3000:3000 -n instantsearchdemo
```

/!\ Don't go on prod with this, if you need to use it on production, you have to create an ingress, change the service for the type LoadBalancer and you need a real URL for the ingress.